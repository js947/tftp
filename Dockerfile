from alpine as builder
run apk add --no-cache build-base git perl xz-dev

workdir /tmp
run git clone --depth=1 https://git.ipxe.org/ipxe.git

workdir /tmp/ipxe/src
run mkdir -p config/local
add chain.ipxe .
add ipxe-config.h config/local/general.h
run make -j 4 bin/undionly.kpxe EMBED=chain.ipxe

from alpine
run apk add --no-cache tftp-hpa
expose 69/udp

entrypoint ["/usr/sbin/in.tftpd"]
cmd ["-L", "--verbose", "--secure", "/srv/tftp"]

run mkdir /srv/tftp
copy --from=builder /tmp/ipxe/src/bin/undionly.kpxe /srv/tftp

add https://boot.netboot.xyz/ipxe/netboot.xyz.kpxe /srv/tftp
add https://boot.netboot.xyz/ipxe/netboot.xyz-undionly.kpxe /srv/tftp
run chmod 644 /srv/tftp/*.kpxe
